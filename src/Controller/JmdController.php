<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\ImportChacal;
use App\Service\ProductLoader;
use Psr\Log\LoggerInterface;

class JmdController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage")
     */
    public function index(): Response
    {
        return $this->render('jmd/index.html.twig', [
            'controller_name' => 'JmdController',
        ]);
    }

    /**
     * @Route("/importchacal", name="app_importchacal")
     * @param Request $request
     * @param string $uploadDir
     * @param FileUploader $uploader
     * @param LoggerInterface $logger
     * @return Response
    */
    public function importchacal(
        Request $request, 
        string $uploadDir, 
        ImportChacal $uploader, 
        LoggerInterface $logger): Response
    {
        $token = $request->get("token");

        if (!$this->isCsrfTokenValid('upload', $token))
        {
            $logger->info("CSRF failure");

            return new Response("Operation not allowed",  Response::HTTP_BAD_REQUEST,
                ['content-type' => 'text/plain']);
        }

        $file = $request->files->get('myfile');

        if (empty($file))
        {
            return new Response("No file specified",
               Response::HTTP_UNPROCESSABLE_ENTITY, ['content-type' => 'text/plain']);
        }

        $filename = $file->getClientOriginalName();
        
        /* Loading fake products */
        $productList = new ProductLoader();
        $products = $productList->loadProductList();

        $selectedOrders = $uploader->processUploadedFile($file, $products);
        
        return $this->render('jmd/orders.html.twig', [
            'products' => $products, 
            'selectedOrders' => $selectedOrders,
        ]);
    }

    /**
     * @Route("/orders", name="app_orderlist")
     */
    public function vieworders(): Response
    {
        return $this->render('jmd/orders.html.twig', [
            'controller_name' => 'JmdController',
        ]);
    }
}
