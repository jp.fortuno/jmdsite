<?php

namespace App\Service;
use App\Entity\Product;

class ProductLoader
{
    public function loadProductList()
    {
        $arr = [
            ["F43010-09010-E", "Apples", 123],
            ["F11139-09010-E", "Oranges", 156],
            ["F39001-09010-E", "Kiwis", 13],
            ["F26045-09010-E", "Mangoes", 103],
            ["F37020-09010-E", "Pineapples", 333],
            ["F11078-09010-E", "Grapes", 227],
            ["F12190-09010-E", "Peaches", 125],
            ["F14150-09010-E", "Goyavas", 826]
        ];

        $productList = [];

        foreach ($arr as $p) {
            $p = new Product($p[0], $p[1], $p[2]);
            $productList[] = $p;

        }

        return $productList;
    }
}
