<?php

namespace App\Service;

use App\Entity\Product;
use App\Service\ProductLoader;
use Psr\Log\LoggerInterface;

class ImportChacal
{
    private $logger;
    private $file;
    private $products;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $products = new ProductLoader();
        $this->products = $products->loadProductList();
    }

    public function upload($uploadDir, $file, $filename)
    {
        try {
            $file->move($uploadDir, $filename);
        } catch (FileException $e){

            $this->logger->error('failed to upload image: ' . $e->getMessage());
            throw new FileException('Failed to upload file');
        }
    }

    public function processUploadedFile($file, $products)
    {
        /* read CSV */
        $file = fopen($file, 'r');
        $lineNumber = 1;

        // push { 'id': 'id', 'valid', '1'} for valid entries
        // push { 'id': 'error message', 'valid', '0'} for invalid entries
        $selectedIds = [];

        while (($line = fgetcsv($file)) !== FALSE) {
           
           $selectedIds[] = $this->validateLine($line, $lineNumber);

           $lineNumber += 1;
        }
        fclose($file);

        return $selectedIds;
    }

    private function validateLine($line, $lineNumber)
    {
        //ignore first line
        if ($lineNumber === 0) {
            return null;
        }

        if (!isset($line) || trim($line[0]) === '') {
            //log this empty line
            return ['empty line at line: '.$lineNumber,'0'];
        }
        
        $id = explode("\t",$line[0])[0];
        
        if (!(substr($id,0,1) == 'F' && strlen($id) == 14)) 
        {
            return ['invalid id format at line: '.$lineNumber, '0'];
        }
        
        foreach ($this->products as $p)
        {
            if ($id === $p->getId())
            {
                return [$id, '1'];
            }
        }
        
        return ['no match found at line: '.$lineNumber, '0'];
    }
}
