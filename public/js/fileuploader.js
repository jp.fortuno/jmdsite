$(document).ready(function() {
    $('#fileUploader').on('click', function() {
        $('#fileUploaded').trigger('click');
    });

    $('fileUploaded').change(function() {
        formdata = new FormData();

        if($(this).prop('files').length > 0)
        {
            formdata = $(this).prop('files')[0];
        }

        var importurl = "{{ path('app_importchacal') }}";
        
        jQuery.ajax({
            url: importurl,
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (result) {
                console.log(result);
            }
        });
    });
});
